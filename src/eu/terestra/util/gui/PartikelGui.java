package eu.terestra.util.gui;

import eu.terestra.util.Main;
import eu.terestra.util.utils.SkullFactory;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;

public class PartikelGui implements Listener {

    private static String inv_name = "Partiekl Gui";

    public enum Partikel {
        Heart, Witch, Flame, Drip
    }

    public static HashMap<Player,Partikel> active = new HashMap<>();

    public static void setActive(Player p, Partikel particel){
        if(active.containsKey(p)){
            active.remove(p);
        }
        active.put(p, particel);
    }

    public static void removeActive(Player p){
        if(active.containsKey(p)){
            active.remove(p);
        }
    }

    public static Partikel getActive(Player p){
        if(active.containsKey(p)){
            return active.get(p);
        }
        return null;
    }

    public static boolean isActive(Player p){
        if(active.containsKey(p)){
            return true;
        }
        return false;
    }


    public static void openGui(Player p){

        Inventory inv = Bukkit.createInventory(null, 9, inv_name);

        ItemStack is_heart = SkullFactory.getSkull("http://textures.minecraft.net/texture/76fdd4b13d54f6c91dd5fa765ec93dd9458b19f8aa34eeb5c80f455b119f278");

        ItemStack is_flame = SkullFactory.getSkull("http://textures.minecraft.net/texture/4080bbefca87dc0f36536b6508425cfc4b95ba6e8f5e6a46ff9e9cb488a9ed");

        ItemStack is_witch = SkullFactory.getSkull("http://textures.minecraft.net/texture/20e13d18474fc94ed55aeb7069566e4687d773dac16f4c3f8722fc95bf9f2dfa");

        ItemStack is_close = new ItemStack(Material.BARRIER);

        p.openInventory(inv);

        new BukkitRunnable() {
            @Override
            public void run() {

                inv.setItem(0, is_heart);
                inv.setItem(1, is_flame);
                inv.setItem(2, is_witch);
                inv.setItem(8, is_close);

                p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 10F, 10F);

            }
        }.runTaskLaterAsynchronously(Main.instance, 10);



    }

    @EventHandler
    public static void onClick(InventoryClickEvent e){

        if(e.getClickedInventory().getName().equalsIgnoreCase(inv_name)){
            if(!e.equals(null)){
                Player p = (Player)e.getWhoClicked();
                if(e.getSlot() == 0){
                    setActive(p, Partikel.Heart);
                    p.sendMessage(Main.prefix + "Unter deinen Füßen sieht man nun Herzen!");
                }else
                if(e.getSlot() == 1){
                    setActive(p, Partikel.Flame);
                    p.sendMessage(Main.prefix + "Unter deinen Füßen sieht man nun Flammen!");
                }else
                if(e.getSlot() == 2){
                    setActive(p, Partikel.Witch);
                    p.sendMessage(Main.prefix + "Unter deinen Füßen sieht man nun Magie!");
                }else
                if(e.getSlot() == 3){
                    setActive(p, Partikel.Drip);
                    p.sendMessage(Main.prefix + "Unter deinen Füßen sieht man nun Drips!");
                }else
                if(e.getSlot() == 8){
                    removeActive(p);
                    p.sendMessage(Main.prefix + "Unter deinen Füßen sieht man nun Nichts!");
                }
                e.getView().close();
                e.setCancelled(true);

            }
        }

    }

}
