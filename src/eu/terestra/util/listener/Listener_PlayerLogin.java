package eu.terestra.util.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerLoginEvent;

public class Listener_PlayerLogin implements Listener{

    @EventHandler
    public static void onKick(PlayerLoginEvent e){
        if (e.getResult() == PlayerLoginEvent.Result.KICK_FULL) {
            e.allow();
        }
    }

}
