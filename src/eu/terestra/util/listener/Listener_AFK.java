package eu.terestra.util.listener;

import eu.terestra.util.Main;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.*;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;

public class Listener_AFK implements Listener {

    static ArrayList<Player> afkList = new ArrayList<Player>();
    static HashMap<Player, Integer> afkMinutes = new HashMap();
    static HashMap<Player, String> oldPlayerListNames = new HashMap();


   static public void startTimer(){

        for (Player p : Bukkit.getServer().getOnlinePlayers()) {
            afkMinutes.put(p, Integer.valueOf(0));
        }

       new BukkitRunnable() {
           @Override
           public void run() {

               for (Player p : Bukkit.getServer().getOnlinePlayers())
               {
                   addMinute(p);

                   if (getTime(p) == 3){
                       autoAFK(p);
                   }
               }
           }
       }.runTaskTimerAsynchronously(Main.instance, 0, 1200);

    }


    static void autoAFK(Player p)
    {

        addAFK(p);

    }

    public static void addAFK(Player p){

        oldPlayerListNames.put(p, p.getPlayerListName());

        p.setPlayerListName("[AFK] §m" + p.getName());

        afkList.add(p);
    }

    void delAFK(Player p)
    {
        if (afkList.contains(p))
        {
            p.setPlayerListName((String)this.oldPlayerListNames.get(p));

    }
        resetTime(p);

    }

    static void addMinute(Player p)
    {
        if (afkMinutes.containsKey(p)) {
            afkMinutes.put(p, Integer.valueOf(((Integer)afkMinutes.get(p)).intValue() + 1));
        } else {
            afkMinutes.put(p, Integer.valueOf(1));
        }
    }

    static int getTime(Player p)
    {
        if (afkMinutes.containsKey(p)) {
            return ((Integer)afkMinutes.get(p)).intValue();
        }
        return 0;
    }

    void resetTime(Player p)
    {
        afkMinutes.put(p, Integer.valueOf(0));
    }


    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e)
    {
        resetTime(e.getPlayer());
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e)
    {
        afkMinutes.remove(e.getPlayer());
    }

    @EventHandler
    public void onMove(PlayerMoveEvent e)
    {

    }

    @EventHandler
    public void onPlayerInteractEntity(PlayerInteractEntityEvent e)
    {
        delAFK(e.getPlayer());
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e)
    {
        delAFK(e.getPlayer());
    }

    @EventHandler
    public void onPlayerBedEnter(PlayerBedEnterEvent e)
    {
        delAFK(e.getPlayer());
    }

    @EventHandler
    public void onChangedWorld(PlayerChangedWorldEvent e)
    {
        delAFK(e.getPlayer());
    }

    @EventHandler
    public void onPlayerEditBook(PlayerEditBookEvent e)
    {
        delAFK(e.getPlayer());
    }

    @EventHandler
    public void onPlayerDropItem(PlayerDropItemEvent e)
    {
        delAFK(e.getPlayer());
    }

    @EventHandler
    public void onPlayerPickupItem(PlayerPickupItemEvent e)
    {
        delAFK(e.getPlayer());
    }

    @EventHandler
    public void onPlayerItemBreak(PlayerItemBreakEvent e)
    {
        delAFK(e.getPlayer());
    }

    @EventHandler
    public void onPlayerShearEntity(PlayerShearEntityEvent e)
    {
        delAFK(e.getPlayer());
    }

    @EventHandler
    public void onPlayerToggleFlight(PlayerToggleFlightEvent e)
    {
        delAFK(e.getPlayer());
    }

    @EventHandler
    public void onPlayerToggleSprint(PlayerToggleSprintEvent e)
    {
        delAFK(e.getPlayer());
    }

    @EventHandler
    public void onPlayerToggleSneak(PlayerToggleSneakEvent e)
    {
        delAFK(e.getPlayer());
    }

    @EventHandler
    public void onPlayerUnleashEntity(PlayerUnleashEntityEvent e)
    {
        delAFK(e.getPlayer());
    }

    @EventHandler
    public void onPlayerBucketFill(PlayerBucketFillEvent e)
    {
        delAFK(e.getPlayer());
    }

    @EventHandler
    public void onPlayerBucketEmpty(PlayerBucketEmptyEvent e)
    {
        delAFK(e.getPlayer());
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e)
    {
        delAFK(e.getPlayer());
    }

    @EventHandler
    public void onPlayerExpChange(PlayerExpChangeEvent e)
    {
        delAFK(e.getPlayer());
    }


}
