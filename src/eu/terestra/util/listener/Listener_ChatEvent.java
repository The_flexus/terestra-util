package eu.terestra.util.listener;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class Listener_ChatEvent implements Listener{


    @EventHandler
    public static void onChat(AsyncPlayerChatEvent e){

        String line = e.getMessage();
        line = ChatColor.translateAlternateColorCodes('&', line);
        e.setMessage(line);


    }

}
