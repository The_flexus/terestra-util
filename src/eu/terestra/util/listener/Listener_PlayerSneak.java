package eu.terestra.util.listener;

import eu.terestra.util.gui.ArrowTrailGui;
import eu.terestra.util.gui.PartikelGui;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleSneakEvent;

public class Listener_PlayerSneak implements Listener{

    @EventHandler
    public static void onSneak(PlayerToggleSneakEvent e){

        Player p = e.getPlayer();

        PartikelGui.openGui(p);
    }

}
