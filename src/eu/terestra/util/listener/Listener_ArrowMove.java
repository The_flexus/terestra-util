package eu.terestra.util.listener;

import eu.terestra.util.Main;
import eu.terestra.util.gui.ArrowTrailGui;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class Listener_ArrowMove implements Listener{

    @EventHandler
    public static void onMove(ProjectileLaunchEvent e){

        if(e.getEntityType() == EntityType.ARROW) {
            if (e.getEntity().getShooter() instanceof Player) {
                Player p = (Player)e.getEntity().getShooter();
                if (ArrowTrailGui.isActive(p)) {

                    new BukkitRunnable() {
                        @Override
                        public void run() {

                            Location loc = e.getEntity().getLocation();

                            if(ArrowTrailGui.getActive(p).equals(ArrowTrailGui.Partikel.Flame)){
                                loc.getWorld().spawnParticle(Particle.LAVA, loc, 10);
                            }else
                            if(ArrowTrailGui.getActive(p).equals(ArrowTrailGui.Partikel.Heart)){
                                loc.getWorld().spawnParticle(Particle.HEART, loc, 10);
                            }else
                            if(ArrowTrailGui.getActive(p).equals(ArrowTrailGui.Partikel.Witch)){
                                loc.getWorld().spawnParticle(Particle.SPELL_WITCH, loc, 10);
                            }else
                            if(ArrowTrailGui.getActive(p).equals(ArrowTrailGui.Partikel.Drip)){
                                loc.getWorld().spawnParticle(Particle.DRIP_LAVA, loc, 10);
                            }

                            if (e.getEntity().isDead() || e.getEntity().isOnGround()) {
                                cancel();
                            }
                        }
                    }.runTaskTimer(Main.instance, 0, 1);
                }
            }
        }
    }

}
