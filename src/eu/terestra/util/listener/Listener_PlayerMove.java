package eu.terestra.util.listener;

import eu.terestra.util.gui.PartikelGui;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class Listener_PlayerMove implements Listener {

    @EventHandler
    public static void onMove(PlayerMoveEvent e ){

        Player p = e.getPlayer();

        if(PartikelGui.isActive(p) == true){

            Location loc = p.getLocation().subtract(0,0.5,0);

            if(PartikelGui.getActive(p).equals(PartikelGui.Partikel.Flame)){
                loc.getWorld().spawnParticle(Particle.LAVA, loc, 10);
            }else
            if(PartikelGui.getActive(p).equals(PartikelGui.Partikel.Heart)){
                loc.getWorld().spawnParticle(Particle.HEART, loc, 10);
            }else
            if(PartikelGui.getActive(p).equals(PartikelGui.Partikel.Witch)){
                loc.getWorld().spawnParticle(Particle.SPELL_WITCH, loc, 10);
            }else
            if(PartikelGui.getActive(p).equals(PartikelGui.Partikel.Drip)){
                loc.getWorld().spawnParticle(Particle.DRIP_LAVA, loc, 10);
            }

        }

    }

}
