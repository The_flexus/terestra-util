package eu.terestra.util.listener;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;

public class Listener_SignEdit implements Listener {


    @EventHandler
    public static void onSign(SignChangeEvent e){

        for (int i = 0; i <= 3; i++){
            String line = e.getLine(i);
            line = ChatColor.translateAlternateColorCodes('&', line);
            e.setLine(i, line);
        }

    }


}
