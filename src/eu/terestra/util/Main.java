package eu.terestra.util;

import eu.terestra.util.gui.ArrowTrailGui;
import eu.terestra.util.gui.PartikelGui;
import eu.terestra.util.listener.*;
import eu.terestra.util.utils.Glow;
import org.bukkit.Bukkit;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.Field;

public class Main extends JavaPlugin
{

    public static Main instance;
    public static String prefix = "§7Terestra >> ";

    @Override
    public void onEnable() {
        instance = this;

        registerGlow();

        PluginManager pm = Bukkit.getPluginManager();

        pm.registerEvents(new Listener_ArrowMove(), this);
        pm.registerEvents(new Listener_PlayerSneak(), this);
        pm.registerEvents(new Listener_PlayerMove(), this);
        pm.registerEvents(new Listener_SignEdit(), this);
        pm.registerEvents(new Listener_ChatEvent(), this);
        pm.registerEvents(new Listener_PlayerLogin(), this);
        pm.registerEvents(new Listener_AFK(), this);

        pm.registerEvents(new ArrowTrailGui(), this);
        pm.registerEvents(new PartikelGui(), this);

        Listener_AFK.startTimer();

    }

    @Override
    public void onDisable() {

    }


    public void registerGlow() {
        try {
            Field f = Enchantment.class.getDeclaredField("acceptingNew");
            f.setAccessible(true);
            f.set(null, true);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        try {
            Glow glow = new Glow(88);
            Enchantment.registerEnchantment(glow);
        }
        catch (IllegalArgumentException e){
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

}
